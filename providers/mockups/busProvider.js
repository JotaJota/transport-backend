	var item1 = {
		route_id: "1",
		route_short_name: "L1",
		route_long_name: "Parque del Sur - Alameda Principal - San Andres",
		route_type: 3
	};
	var item2 = {
		route_id: "2",
		route_short_name: "L2",
		route_long_name: "Parque del Sur - Alameda Principal - San Andres",
		route_type: 3
	};
	var item3 = {
		route_id: "3",
		route_short_name: "L3",
		route_long_name: "Parque del Sur - Alameda Principal - San Andres",
		route_type: 3
	};

var BusProviderMockup = {

	listAllBuslines: function(callback) {
		callback(null,[item1, item2, item3]);

	}
}

module.exports = BusProviderMockup;