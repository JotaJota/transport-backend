
var commonCb = function(cb,err,data) {
		if (err) console.log(err.toString());

		if (err) return cb(err,null);
		else return cb(null,data);
	};

var allQuery = function(db,cb,queryString,params) {
			db.all(queryString, params,
				function(err, data) {
					commonCb(cb,err,data);
				});
	};

var getQuery = function(db,cb,queryString,params) {
			db.get(queryString, params,
				function(err, data) {
					commonCb(cb,err,data);
				});
	};

var	processTimes =  function(times) {
		var result;

		if (times && times.length > 0 && times[0].hasOwnProperty('direction')) {
			result = { oneWay: { headsign: "", times: [] }, returnWay:  { headsign: "", times: [] }};

			for (var i = 0; i < times.length; i++) {
				if (times[i].direction === 0) {
					result.oneWay.times.push(times[i].arrival_time);
					if (!result.oneWay.headsign) result.oneWay.headsign = times[i].headsign;
				} else {
					result.returnWay.times.push(times[i].arrival_time);
					if (!result.returnWay.headsign) result.returnWay.headsign = times[i].headsign;
				}
			}
		} else {
			result = [];
			for (var i = 0; i < times.length; i++) 
				result.push(times[i].arrival_time);
		}

		return result;
	};


const LAT_DEGREE_IN_METRES = 110959.0; //around Malaga	
const LON_DEGREE_IN_METRES = 90164.0; //around Malaga	

var getSquare = function(lat,lon,radius) {
	var radiusInLatDeg = radius / LAT_DEGREE_IN_METRES;
	var radiusInLonDeg = radius / LON_DEGREE_IN_METRES;

	var coordinates =  	{ 
							start: 
								{	
									lat: (lat - radiusInLatDeg), 
									lon: (lon - radiusInLonDeg)
								},
							end:
								{
									lat: (Number(lat) + radiusInLatDeg),
									lon: (Number(lon) + radiusInLonDeg)
								}
						};

	return coordinates;
}

module.exports = {
	commonCb,
	allQuery,
	getQuery,
	processTimes,
	getSquare
};
