var db = require('./busDBQueries');
var aux = require('./auxFunctions');
var async = require('async');


var BusProviderMalaga = {
	listAllBuslines: function(callback) {
		var result = [];

		db.getBuslines(function(err,buslines) {

			if (aux.errorCheck(callback,err,buslines,'Error: Data not found')) return;

			// all right process and return data
			for (var i = 0; i < buslines.length; i++) {
				busline = buslines[i];
				result.push(aux.processBusline(busline));
			}
			callback(null,result);
		});
	},

	listBuslineById: function(id, callback) {
		var result;

		db.getBusline(id, function(err,busline) {
			if (aux.errorCheck(callback,err,busline,'Error: Busline not found')) return;
			// no errors, busline found, return data
			callback(null,aux.processBusline(busline));
			});
	},

	listBuslinesByStopId: function(stopid, callback) {
		db.getBuslinesByStopId(stopid,function (err,data) {
			if (aux.errorCheck(callback,err,data,'Error: Stop not found')) return;
			callback(null,data);
		});
	},

	listStopsByBusId: function(busid, callback) {
		var result = {
				routeId: parseInt(busid),
				oneWay: {
					headsign: "",
					numStops: 0,
					stops:[]
				},
				returnWay: {
					headsign: "",
					numStops: 0,
					stops: []
				}	
			};
		var nStopsOW = 0;
		var nStopsRet = 0;	

		db.getStopsByBusId(
			busid, 
			function(err,stops){
				var owHeadsign;
				var rwHeadsign;

				if (aux.errorCheck(callback,err,stops,'Error: Bus line not found')) return;
				//all right process and return data
				for (var i = 0; i < stops.length; i++) {
					var stop = stops[i];

					if (stop.direction_id) { //RETURN
						if(!rwHeadsign) rwHeadsign = stop.trip_headsign;
						nStopsRet++;
						result.returnWay.stops.push(aux.processStop(stop));
					} else {					//ONE WAY 
						if(!owHeadsign) owHeadsign = stop.trip_headsign;
						nStopsOW++;
						result.oneWay.stops.push(aux.processStop(stop));
					}
				}
				result.oneWay.numStops = nStopsOW;
				result.oneWay.headsign = owHeadsign;
				result.returnWay.numStops = nStopsRet;
				result.returnWay.headsign = rwHeadsign;
				callback(null,result);	
			});
	},

	listStopById: function(id, callback) {
		var result;

		db.getStop(id, 
			function(err,stop) {
				if (aux.errorCheck(callback,err,stop,'Error: Bus stop not found')) return;
				// all right, return data
				callback(null,aux.processStop(stop));
			});
	},

	listTimesByStopIdAndBusId: function(stopid, busid, duration, callback) {

		db.getStopTimesForBusline(stopid,busid,duration, 
			function(err, times) {
				if (aux.errorCheck(callback,err,times,'Error: Data not found')) return;
				callback(null,times);
			});
	},
	
	listTimesByStopId: function(stopid, duration, callback) {
		var result = {
			stopId: 0,
			stopName: "",
			stopLat: 0,
			stopLon: 0,
			lines: []
		};

		db.getBuslinesByStopId(stopid,function (err,buslines){
			if (aux.errorCheck(callback,err,buslines,'Error: Data not found')) return;

			db.getStop(stopid, 
				function(err,stop) {
				if (aux.errorCheck(callback,err,stop,'Error: Data not found')) return;

					result.stopId = stop.stop_id;
					result.stopName = stop.stop_name;
					result.stopLat = stop.stop_lat;
					result.stopLon = stop.stop_lon;

					async.forEachOf(
						buslines, //first arg
						function(busline,i,callbackEach) { // second arg
							db.getStopTimesForBusline(stopid,busline.route_id, duration,
								function(err,times) {
									if (err) {
										callbackEach(err);
										return;
									}
									result.lines[i] = { 
													routeId: busline.route_id,
													routeShortName: busline.route_short_name,
													routeLongName: busline.route_long_name,
													times: times
												};
									callbackEach(null);
								});
						},
						function (err) { //third arg
							if(err){
								callback(aux.newError(500,err),null);
								return;
							}
							callback(null,result);
						});
				});
		});
	},

	listNearestStops: function(lat,lon,radius,callback) {
		var result = [];

		db.getNearestStops(lat,lon,radius, function(err,stops) {
			if (aux.errorCheck(callback,err,stops,'Error: Data not found')) return;

			for (var i = 0; i < stops.length; i++) {
				result.push(aux.processStop(stops[i]));
			}
			callback(null,result);
		});
	}
}

module.exports = BusProviderMalaga;