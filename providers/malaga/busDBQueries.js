
var fs = require('fs')
var storageFile = "./data/BUSMALAGA.db"
var exists = fs.existsSync(storageFile);
var dbaux = require("./dbAuxFunctions");
var aux = require('./auxFunctions');

if (!exists) {
	//initialize database reading the csv
	console.log("Database storage file not found.");
}

var sqlite = require('sqlite3').verbose();

var db = new sqlite.Database(storageFile, sqlite.OPEN_READONLY, function(err) {
	if (err) {
		console.log("ERROR opening the database:\n" + err);
	}
});


busDBQueries = {
	getBuslines: function (callback) {
		dbaux.allQuery(db,callback,"SELECT * FROM rutas ORDER BY 1",[]);
	},

	getBusline: function (routeid, callback) {
		dbaux.getQuery(db,callback,"SELECT * FROM RUTAS WHERE route_id = ?",[routeid]);
	},

	getBuslinesByStopId: function(stopid,callback) {
		var queryString = "SELECT distinct(rutas.route_id), \
		rutas.route_short_name, \
		rutas.route_long_name \
		FROM rutas \
		INNER JOIN servicios ON rutas.route_id = servicios.route_id \
		INNER JOIN horarios ON horarios.trip_id = servicios.trip_id \
		WHERE horarios.stop_id = ?";

		dbaux.allQuery(db,callback,queryString,[stopid]);
	},

	getStop: function(stopid,callback) {
		dbaux.getQuery(db,callback,"SELECT * FROM PARADAS WHERE stop_id = ?", [stopid]);
	},

	getStopTimesForBusline: function(stopid, busid, duration, callback) {
		var currentDate = new Date();
		var date = aux.yyyymmdd(currentDate);
		var today = aux.getDay(currentDate);

		var servicesTodaySubQueryString = 
				"SELECT service_id FROM calendario WHERE " + today + " = 1 " +
				"AND date('" + date + "') >= start_date AND date('" + date + "') <= end_date";

		var queryString = 
		"SELECT distinct(horarios.arrival_time) \
		FROM horarios \
		INNER JOIN servicios ON horarios.trip_id = servicios.trip_id \
		WHERE horarios.stop_id = ? \
		AND servicios.route_id = ? \
		AND servicios.service_id IN (" + servicesTodaySubQueryString + ") ";
		
		var queryStringTimeRestriction = 
		"AND horarios.arrival_time >= time('NOW','localtime') ";

		var queryStringLimit =
		"ORDER BY 1 LIMIT 3";

		var queryStringDuration = 
		"AND horarios.arrival_time <= time('NOW','localtime','+" + duration + " minutes') \
		ORDER BY 1";

		db.all(queryString + queryStringTimeRestriction + queryStringDuration, stopid, busid, 
			function(err,times) {
				if (err) {
					callback(err,null);
					return;
				} 
				if (!times || 0 === times.length) {
					db.all(queryString + queryStringTimeRestriction + queryStringLimit, stopid, busid,
						function (err,times) {
							if (err) {
								callback(err,null);
								return;
							}
							if (!times || 0 === times.length) {
								db.all(queryString + queryStringLimit, stopid, busid,
									function(err,times) {
										dbaux.commonCb(callback,err,dbaux.processTimes(times));
									});
							} else {
								callback(null,dbaux.processTimes(times));
							}
						});
				} else callback(null,dbaux.processTimes(times));
		});
	},

	getStopsByBusId: function (busid,callback) {

		 queryString = "SELECT \
		 	distinct(paradas.stop_id), \
			horarios.stop_sequence, \
			servicios.direction_id, \
			servicios.trip_headsign, \
			paradas.stop_name, \
			paradas.stop_lat, \
			paradas.stop_lon \
			FROM horarios \
			INNER JOIN servicios ON horarios.trip_id = servicios.trip_id \
			INNER JOIN paradas ON horarios.stop_id = paradas.stop_id \
			WHERE servicios.route_id = ? \
			ORDER BY 2";

		dbaux.allQuery(db,callback,queryString,[busid]);
	},

	getNearestStops: function (lat,lon,radius,callback) {
		var coordinates = dbaux.getSquare(lat,lon,radius);

		queryString = "SELECT * FROM paradas \
		WHERE stop_lat >= ? AND stop_lat <= ? \
		AND stop_lon >= ? AND stop_lon <= ?";

		dbaux.allQuery(db,callback,queryString,
			[coordinates.start.lat,
			 coordinates.end.lat,
			 coordinates.start.lon,
			 coordinates.end.lon]);

	}
}

module.exports = busDBQueries;