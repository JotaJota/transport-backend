bus = require('./busProvider.js');
metro = require('./metroProvider.js');



var MalagaProviders = {
	listAllBuslines: bus.listAllBuslines,
	listBuslineById: bus.listBuslineById,
	listBuslinesByStopId: bus.listBuslinesByStopId,
	listStopsByBusId: bus.listStopsByBusId,
	listBusStopById: bus.listStopById,
	listBusTimesByStopIdAndBusId: bus.listTimesByStopIdAndBusId,
	listBusTimesByStopId: bus.listTimesByStopId,
	listAllMetrolines: metro.listAllMetrolines,
	listAllMetrolinesWithStations: metro.listAllMetrolinesWithStations,
	listMetrolineById: metro.listMetrolineById,
	listStationById: metro.listStationById,
	listStationsByMetroId: metro.listStationsByMetroId,
	listMetrolinesByStationId: metro.listMetrolinesByStationId,
	listMetroTimesByStationIdAndMetroId: metro.listTimesByStationIdAndMetroId,
	listMetroTimesByStationId: metro.listTimesByStationId,
	listNearestStops: bus.listNearestStops,
	listNearestStations: metro.listNearestStations
}

module.exports = MalagaProviders;