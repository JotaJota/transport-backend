var weekDays = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];

var pad = function(d) {
    return (d < 10) ? '0' + d.toString() : d.toString();
}

var yyyymmdd = function(dateIn) {
	   var yyyy = dateIn.getFullYear();
	   var mm = pad(dateIn.getMonth()+1); // getMonth() is zero-based
	   var dd  = pad(dateIn.getDate());
	   return String( yyyy + "-" + mm + "-" + dd);
	};

var getDay = function(dateIn) {
		return weekDays[dateIn.getDay()];
	};

var newError = function(code,message) {
		return { statusCode: code, message: message };
	};

var emptyData = function(cb,errorStatusCode,errorMessage) {
		return cb(newError(errorStatusCode,errorMessage),null);
	};

var processBusline = function(entry) {
		return {
			routeId: entry.route_id,
			routeShortName: entry.route_short_name,
			routeLongName: entry.route_long_name,
			routeType: entry.route_type
		};
	};

var processMetroline = function(entry, listStations) {
	var result = {
			lineId: entry.line_id,
			lineName: entry.line_name,
			lineShortName: entry.line_short_name,
			lineColor: entry.line_color,
		};
		if (listStations !== "undefined") {
			result.stations = listStations;	
		}
		return result;
	};

var	processStop = function(entry) {
		if (entry.hasOwnProperty('station_id')) { //metro station
			result = {
				stationId: entry.station_id,
				stationName: entry.station_name,
				stationLat: entry.station_lat,
				stationLon: entry.station_lon,
			};
			if (entry.hasOwnProperty('station_sequence'))
				result.stationSequence = entry.station_sequence;

		} else if (entry.hasOwnProperty('stop_id')) {  //bus station
			result = {
				stopId: entry.stop_id, 
				stopName: entry.stop_name,
				stopLatitude: entry.stop_lat,
				stopLongitude: entry.stop_lon
			};
			if (entry.hasOwnProperty('stop_sequence'))
				result.stopSequence = entry.stop_sequence;
		}

		return result;
	};

var errorCheck = function(callback,err,data,errString) {
		// some error exectuing the query
		if (err) {
			callback(newError(500, err), null);
			return true;
		}
		// no results found
		if (!data || 0 === data.length) {
			emptyData(callback, 500, errString);
			return true;
		}
		return false;
	};

module.exports = {yyyymmdd, getDay, newError, emptyData, processBusline, processMetroline, processStop, errorCheck};