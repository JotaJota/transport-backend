var db = require('./metroDBQueries');
var aux = require('./auxFunctions');
var async = require('async');

var MetroProviderMalaga = {

	listAllMetrolines: function(callback) {
		var result = [];

		db.getMetrolines(function(err,metrolines) {
			if (aux.errorCheck(callback,err,metrolines, 'Error: Data not found')) return;

			for (var i = 0; i < metrolines.length; i++) {
				result.push(aux.processMetroline(metrolines[i]));
			}
			callback(null,result);
		});
	},

	listAllMetrolinesWithStations: function(callback) {
		var result = [];

		db.getMetrolines(function(err,metrolines) {
			if (aux.errorCheck(callback,err,metrolines, 'Error: Data not found')) return;

					async.forEachOf(
						metrolines, //first arg
						function(metroline,i,callbackEach) { // second arg

							db.getStationsByMetroId(metroline.line_id,function(err,stations) {
								var tmpStationsList = [];
								result[i] = aux.processMetroline(metroline);
								for (var j = 0; j < stations.length; j++) {
									tmpStationsList[j] = aux.processStop(stations[j]);
								}
								result[i].stations = tmpStationsList;

								// console.log(result);
								callbackEach(null);
							});
						},
						function (err) { //third arg
							if(err){
								callback(aux.newError(500,err),null);
								return;
							}
							callback(null,result);
					});

		});

	},

	listMetrolineById: function(metroid, callback) {
		db.getMetroline (metroid,function(err,metroline) {
			if (aux.errorCheck(callback,err,metroline,'Error: Metroline not found')) return;

			callback(null,aux.processMetroline(metroline));
		});
	},

	listMetrolinesByStationId: function(stationid,callback) {
		var result = [];

		db.getMetrolinesByStationId(stationid,function (err, metrolines) {
			if (aux.errorCheck(callback,err,metrolines,"Error: Data not found")) return;

			for (var i = 0; i < metrolines.length; i++) {
				result.push(aux.processMetroline(metrolines[i]));
			}

			callback(null,result);
		});
	},

	listStationById: function(stationid, callback) {
		db.getStationById(stationid, function (err,stop) {
			if (aux.errorCheck(callback,err,stop,'Error: Stop not found')) return;

			callback(null, aux.processStop(stop));
		});
	},

	listStationsByMetroId: function(metroid, callback) {
		var result = [];

		db.getStationsByMetroId(metroid, function (err, stations) {
			if (aux.errorCheck(callback,err,stations,'Error: Data not found')) return;

			for (var i = 0; i < stations.length; i++) {
				result.push(aux.processStop(stations[i]));
			}
			callback(null,result);
		});
	},

	listTimesByStationIdAndMetroId: function(stationid, metroid, duration, callback) {
		var result = [];

		db.getStationTimesForMetroline(stationid,metroid,duration, function (err,data) {
			if (aux.errorCheck(callback,err,data,'Error: Data not found')) return;
			callback(null,data);
		});

	},

	listTimesByStationId: function(stationid, duration, callback) {
		var result = {};

		db.getMetrolinesByStationId(stationid, function(err,metrolines) {
			if(aux.errorCheck(callback,err,metrolines,"Error: Data not found")) return;
			db.getStationById(stationid,
				function(err,station) {
					if(aux.errorCheck(callback,err,station,"Error: Stop not found")) return;

					result.stationId = station.station_id;
					result.stationName = station.station_name;
					result.stationLat = station.station_lat;
					result.stationLon = station.station_lon;
					result.lines = [];

					async.forEachOf(
						metrolines, //first arg
						function(metroline,i,callbackEach) { // second arg
							db.getStationTimesForMetroline(stationid,metroline.line_id, duration,
								function(err,times) {
									if (err) {
										callbackEach(err);
										return;
									}
									result.lines[i] = { 
													lineId: metroline.line_id,
													lineShortName: metroline.line_short_name,
													lineName: metroline.line_name,
													lineColor: metroline.line_color,
													oneWay: times.oneWay,
													returnWay: times.returnWay
												};
									callbackEach(null);
								});
						},
						function (err) { //third arg
							if(err){
								callback(aux.newError(500,err),null);
								return;
							}
							callback(null,result);
						});

				});
		});
	},

	listNearestStations: function(lat,lon,radius,callback) {
		var result = [];

		db.getNearestStations(lat,lon,radius, function(err,stations) {
			if (aux.errorCheck(callback,err,stations,'Error: Data not found')) return;

			for (var i = 0; i < stations.length; i++) {
				result.push(aux.processStop(stations[i]));
			}
			callback(null,result);
		});	
	}
}

module.exports = MetroProviderMalaga;