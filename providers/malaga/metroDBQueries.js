var fs = require('fs')
var storageFile = "./data/METROMALAGA.db"
var exists = fs.existsSync(storageFile);
var dbaux = require("./dbAuxFunctions")
var aux = require('./auxFunctions');

if (!exists) {
	//initialize database reading the csv
	console.log("Database storage file not found.");
}

var sqlite = require('sqlite3').verbose();

var db = new sqlite.Database(storageFile, sqlite.OPEN_READONLY, function(err) {
	if (err) {
		console.log("ERROR opening the metro database:\n" + err);
	}
});


metroDBQueries = {
	getMetrolines: function (callback) {
		dbaux.allQuery(db,callback,"SELECT * FROM lines ORDER BY 1",[]);
	},

	getMetroline: function (metroid, callback) {
		dbaux.getQuery(db,callback,"SELECT * FROM lines WHERE line_id = ?", [metroid]);
	},

	getMetrolinesByStationId: function (stationid,callback) {

		queryString = "SELECT \
		distinct(lines.line_id), \
		lines.line_name, \
		lines.line_color, \
		lines.line_short_name \
		FROM trips \
		INNER JOIN lines ON lines.line_id = trips.line_id \
		WHERE trips.station_id = ? \
		AND trips.final_station = 0";

		dbaux.allQuery(db,callback,queryString,[stationid]);
	},

	getStationsByMetroId: function (metroid,callback) {

		queryString = "SELECT stations.station_id, \
		stations.station_name, \
		stations.station_lat, \
		stations.station_lon, \
		trips.station_sequence \
		FROM stations \
		INNER JOIN trips \
		ON stations.station_id = trips.station_id \
		WHERE trips.line_id = ? \
		AND direction = 0 \
		ORDER BY 5";

		dbaux.allQuery(db,callback,queryString,[metroid]);
	},

	getStationById: function (stationid, callback) {
		dbaux.getQuery(db,callback,"SELECT * FROM stations WHERE station_id = ?",[stationid]);
	},

	getStationTimesForMetroline: function (stationid,metroid,duration,callback) {

		queryString = "SELECT \
		timetables.arrival_time, \
		trips.direction, \
		trips.headsign \
		FROM timetables \
		INNER JOIN trips ON timetables.trip_id = trips.trip_id \
		WHERE trips.station_id = ? \
		AND trips.final_station = 0 \
		AND trips.line_id = ? ";

		var queryStringTimeRestriction = 
		"AND timetables.arrival_time >=  time('NOW', 'localtime') ";

		var queryStringDuration = 
		"AND timetables.arrival_time <= time('NOW','localtime','+" 
		+ duration + 
		" minutes') ORDER BY 1";

		var queryStringLimit =
		"ORDER BY 1 LIMIT 3";

		db.all(queryString + queryStringTimeRestriction + queryStringDuration, stationid, metroid, 
			function(err,times) {
				if (err) {
					callback(err,null);
					return;
				} 
				if (!times || 0 === times.length) {
					db.all(queryString + queryStringTimeRestriction + queryStringLimit, stationid, metroid,
						function (err,times) {
							if (err) {
								callback(err,null);
								return;
							}
							if (!times || 0 === times.length) {
								db.all(queryString + queryStringLimit, stationid, metroid,
									function(err,times) {
										dbaux.commonCb(callback,err,dbaux.processTimes(times));
									});
							} else {
								callback(null,dbaux.processTimes(times));
							}
						});
				} else callback(null,dbaux.processTimes(times));
		});
	},

	getNearestStations: function (lat,lon,radius,callback) {
		var coordinates = dbaux.getSquare(lat,lon,radius);

		queryString = "SELECT * FROM stations \
		WHERE station_lat >= ? AND station_lat <= ? \
		AND station_lon >= ? AND station_lon <= ?";

		dbaux.allQuery(db,callback,queryString,
			[coordinates.start.lat,
			 coordinates.end.lat,
			 coordinates.start.lon,
			 coordinates.end.lon]);

	}

} 

module.exports = metroDBQueries;