var express = require('express');
var logger = require('morgan');
var bodyParser = require('body-parser');

var app = express();



app.use(logger('dev'));
app.use(bodyParser.json());

app.get('/',function(req,res) {
	res.send('test');
});

var imp = require('./providers/malaga/malagaProviders');

require('./routes/facade')(app, imp);

app.on('listening', function(){
	console.log('Server listening');
})

app.listen(8080);
console.log('Server listening on port 8080');
