
const TIMES_REQ_DEFAULT_DURATION = 60;
const DEFAULT_NEAREST_STATIONS_RADIUS = 500;

module.exports = function(app,imp) {
	function listAllMetrolines(req,res) {
		imp.listAllMetrolines(function (err,data) {
			processResponse(res,err,data);
		});
	}	

	function listAllMetrolinesWithStations(req,res) {
		imp.listAllMetrolinesWithStations(function (err,data) {
			processResponse(res,err,data);
		});
	}	

	function listMetrolineById(req,res) {
		var metroid = req.params.metroid;

		imp.listMetrolineById(metroid, function(err,data) {
			processResponse(res,err,data);
		});
	}

	function listStationById(req,res) {
		var stationid = req.params.stationid;

		imp.listStationById(stationid, function(err,data) {
			processResponse(res,err,data);
		});
	}

	function listStationsByMetroId(req,res) {
		var metroid = req.params.metroid;

		imp.listStationsByMetroId(metroid, function(err,data) {
			processResponse(res,err,data);
		});
	}

	function listMetrolinesByStationId(req,res) {
		var stationid = req.params.stationid;

		imp.listMetrolinesByStationId(stationid, function(err,data) {
			processResponse(res,err,data);
		});
	}

	function listTimesByStationIdAndMetroId(req,res) {
		var stationid = req.params.stationid;
		var metroid = req.params.metroid;
		var duration = req.params.duration || TIMES_REQ_DEFAULT_DURATION;

		imp.listMetroTimesByStationIdAndMetroId(stationid, metroid, duration, function(err,data) {
			processResponse(res,err,data);
		});
	}

	function listTimesByStationId(req,res) {
		var stationid = req.params.stationid;
		var duration = req.params.duration || TIMES_REQ_DEFAULT_DURATION;

		imp.listMetroTimesByStationId(stationid, duration, function(err,data) {
			processResponse(res,err,data);
		});
	}

	function listNearestStations(req,res) {
		var lat = req.params.lat;
		var lon = req.params.lon;
		var radius = req.params.radius || DEFAULT_NEAREST_STATIONS_RADIUS;

		imp.listNearestStations(lat,lon,radius,function(err,data) {
			processResponse(res,err,data);
		});
	}


	//RUTAS
	app.get('/metrolines', listAllMetrolines);
	app.get('/metrolines/stations', listAllMetrolinesWithStations);
	app.get('/metroline/:metroid', listMetrolineById);
	app.get('/metroline/:metroid/stations', listStationsByMetroId);
	app.get('/station/:stationid', listStationById);
	app.get('/station/:stationid/metrolines', listMetrolinesByStationId);
	app.get('/station/:stationid/metroline/:metroid/times', listTimesByStationIdAndMetroId);
	app.get('/station/:stationid/times', listTimesByStationId);
	app.get('/station/:stationid/metroline/:metroid/times/:duration', listTimesByStationIdAndMetroId);
	app.get('/station/:stationid/times/:duration', listTimesByStationId);
	app.get('/stations/nearest/:lat/:lon/:radius', listNearestStations);
	app.get('/stations/nearest/:lat/:lon/', listNearestStations);
}
