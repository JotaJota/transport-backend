const DEFAULT_TIMES_REQ_DURATION = 60;
const DEFAULT_NEAREST_STOPS_RADIUS = 500;


module.exports = function(app,imp) {

	function listAllBuslines(req,res) {
		imp.listAllBuslines( function(err,data) {
			processResponse(res,err,data);
		});
	}

	function listBuslineById(req,res) {
		var busid = req.params.id;

		imp.listBuslineById(busid, function(err,data) {
			processResponse(res,err,data);
		});
	}

	function listBuslinesByStopId(req,res) {
		var stopid = req.params.stopid;	

		imp.listBuslinesByStopId(stopid, function(err,data) {
			processResponse(res,err,data);
		});
	}

	function listStopsByBusId(req,res) {
		var busid = req.params.busid;

		imp.listStopsByBusId(busid, function(err,data) {
			processResponse(res,err,data);
		});
	}

	function listStopById(req,res) {
		var stopid = req.params.id;

		imp.listBusStopById(stopid, function(err,data) {
			processResponse(res,err,data);
		});
	}

	function listTimesByStopId(req,res) {
		var stopid = req.params.stopid;	
		var duration = req.params.duration || DEFAULT_TIMES_REQ_DURATION;

		imp.listBusTimesByStopId(stopid, duration, function(err,data) {
			processResponse(res,err,data);
		});
	}

	function listTimesByStopIdAndBusId(req,res) {
		var stopid = req.params.stopid;	
		var busid = req.params.busid;
		var duration = req.params.duration || DEFAULT_TIMES_REQ_DURATION;

		imp.listBusTimesByStopIdAndBusId(stopid, busid, duration, function(err,data) {
			processResponse(res,err,data);
		});
	}

	function listNearestStops(req,res) {
		var lat = req.params.lat;
		var lon = req.params.lon;
		var radius = req.params.radius || DEFAULT_NEAREST_STOPS_RADIUS;

		imp.listNearestStops(lat,lon,radius, function(err,data) {
			processResponse(res,err,data);
		});
	}

	//RUTAS
	app.get('/buslines', listAllBuslines);
	app.get('/busline/:id', listBuslineById);
	app.get('/busline/:busid/stops/', listStopsByBusId);
	app.get('/stop/:id', listStopById);
	app.get('/stop/:stopid/buslines', listBuslinesByStopId);
	app.get('/stop/:stopid/times', listTimesByStopId);
	app.get('/stop/:stopid/busline/:busid/times', listTimesByStopIdAndBusId);
	app.get('/stop/:stopid/times/:duration', listTimesByStopId);
	app.get('/stop/:stopid/busline/:busid/times/:duration', listTimesByStopIdAndBusId);
	app.get('/stops/nearest/:lat/:lon/:radius', listNearestStops);
	app.get('/stops/nearest/:lat/:lon', listNearestStops);

}

