module.exports = function(app, imp) {

	sendMessage = function(res, status, message) {
		console.log('Ups, there was a ' + status + ' error with the message ' + message);
		res.status(status).send({
			success: false,
			message: message
		});
	}

	processResponse = function(res,err,data) {
		if (err) {
			//error 
			sendMessage(res,err.statusCode, err.message);
			return;
		}
		res.json(data);
	}

	require('./bus')(app, imp);
	require('./metro')(app, imp);
	//require('./bike')(app, imp);
}
